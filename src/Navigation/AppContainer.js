import {createAppContainer} from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import HomeScreen from '../screen/HomeScreen'
import DetailScreen from '../screen/DetailScreen'
import {
  GALLERY_HOME,
  GALLERY_DETAIL
} from '../routes'




export const AppContainer = createAppContainer(
  createStackNavigator({
    GALLERY_HOME: {screen: HomeScreen},
    GALLERY_DETAIL: {
    screen: DetailScreen
    },
  },
    {
        defaultNavigationOptions: {
                headerTitleAlign: 'left | center',
                headerStyle: {
                    backgroundColor: '#FFF4E5',
                },
                headerTintColor: '#4F3F31',
                },
    },
  {
     initialRouteName: GALLERY_HOME,

  },),)


