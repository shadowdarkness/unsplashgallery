import React, { PureComponent } from 'react';
import { View } from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import ImageFullSize from '../components/ImageFullSize'

export default class DetailScreen extends PureComponent {
    static navigationOptions = {
                title: 'Detail'}



  render() {
    const {urls,alt_description} = this.props.navigation.state.params
    const data =  {urls,alt_description}
    console.log('data', data)
     return (
        <View>
            <ImageFullSize data={data} />
        </View>
    )
  }
}

