import React, {Component} from 'react';
import {View,ScrollView } from 'react-native';
import ImageList from '../components/ImageList'
import {GALLERY_DETAIL}  from '../routes'
import  {connect} from 'react-redux'
import {getImage} from '../store/action'




class HomeScreen extends Component {
    static navigationOptions = {headerTitle: 'Gallery'}


    componentDidMount = async () => {
        this.props.getImage()
    }


    render() {
        const {navigation,data} = this.props
        console.log('this.props', this.props)
        return (
          <View>
            <ScrollView>
              <View>
                {data.map(item => (
                  <ImageList
                  data={item}
                  key={item.id}
                  onPress={() => navigation.navigate(GALLERY_DETAIL, (item) )}/>
                ))}

              </View>
            </ScrollView>
          </View>
        )
      }
    }
    const mapStateToProps = state => {
        return {
            data: state.dataReducer.data,
            isLoading: state.dataReducer.isLoading
        }
    }
export default connect(mapStateToProps, {getImage})(HomeScreen)

