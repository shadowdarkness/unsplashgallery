import React from 'react'
import {Provider} from 'react-redux'
import {createStore,applyMiddleware,combineReducers} from'redux'
import thunk from 'redux-thunk'
import routes from  '../routes'
import {imageReducer} from './reducer'

const middleware = applyMiddleware(thunk)

const rootReducer = combineReducers({
    dataReducer: imageReducer,

})

export const store = createStore(rootReducer,middleware)

