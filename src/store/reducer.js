import {IMAGE_LOAD_SUCCEED,IMAGE_REQUEST,IMAGE_LOAD_FAIL} from './types'


const INITIAL_STATE = {
    isLoading:false,
    data: [],
    error_message: ""


}
export  const imageReducer = (state = INITIAL_STATE,action) => {
    switch(action.type) {
        case IMAGE_REQUEST:
            return{
                ...state,
                isLoading: true
            }
        case IMAGE_LOAD_SUCCEED:
            return{
                ...state,
                isLoading: false,
                data: action.payload
            }
        case IMAGE_LOAD_FAIL:
            return{
                ...state,
                isLoading: false,
                error_message: action.payload
            }
        default: {
        return state
        }
        }
    }


