import React from 'react'
import { View,Image,StyleSheet } from 'react-native'


const ImageFullSize = ({ data}) => {
  const {h1,cover, container} = styles
  const {urls} = data
  return (
    <View>
        <Image style={cover} source={{ uri: urls.regular }} />
    </View>
  )
}





const styles = StyleSheet.create({
    cover:{
        width: '100%',
        height: '100%',
        borderRadius: 10

    }
})



export default ImageFullSize