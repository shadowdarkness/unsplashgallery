import React from 'react'
import { View,Text,Image,StyleSheet,TouchableOpacity } from 'react-native'
import {h,w} from './constants';

const ImageList = ({ data, onPress }) => {
  const {h1,cover, container} = styles
  const { urls , user,description,alt_description} = data
  return (
    <TouchableOpacity onPress = {onPress}>
        <View style={container}>
         <View>
            <Image style={cover} source={{ uri: urls.small }} />
         </View>
        <Text style={h1}>{alt_description} </Text>
        <Text style={h1}>Author : {user.name} </Text>

        </View>
    </TouchableOpacity>
  )
}





const styles = StyleSheet.create({
    container: {
        shadowColor: '#000',
        shadowRadius: 8,
        shadowOffset: {width: 0, height: 5},
        shadowOpacity: 0.4,
        justifyContent: 'center',
        flex: 1,
        backgroundColor: '#FFF4E5'

    },
    h1: {
        fontFamily: Platform.OS ===
        'android'
        ? 'monospace'
        : 'Helvetica'
        ,
        alignSelf: 'center',
        textAlign: 'center',
        width: w / 2.4

    },
    cover:{
        width: '100%',
        height: w * 0.5,
        borderRadius: 10

    }
})



export default ImageList